package yandex.evgeny.yandexapplication1;

import android.app.Application;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

public class MyApplication extends Application {
    private static final String TAG = "MyApplication";

    public void onCreate()
    {
        super.onCreate();
        Log.i(TAG, "qwer.onCreate()");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "qwer.onConfigurationChanged()");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i(TAG, "qwer.onLowMemory()");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.i(TAG, "qwer.onTerminate()");
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.i(TAG, "qwer.onTrimMemory()");
    }
}
