package yandex.evgeny.yandexapplication1;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RadioGroup;

public class MyRadioGroup extends RadioGroup {
    private static final String TAG = "MyRadioGroup";

    public MyRadioGroup(Context context) {
        super(context);
        Log.i(TAG, "qwer.ConstructorCalled");
    }

    public MyRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.i(TAG, "qwer.ConstructorCalled");
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.i(TAG, "qwer.onAttachedToWindow()");
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.i(TAG, "qwer.onDetachedFromWindow()");
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Log.i(TAG, "qwer.onLayout()");
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.i(TAG, "qwer.onSizeChanged()");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.i(TAG, "qwer.onDraw()");
    }
}
