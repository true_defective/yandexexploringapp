package yandex.evgeny.yandexapplication1;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private TextView textView;

    public void changeText(View view) throws Exception {
        textView.setText("radio button checked" + view.getId());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.text_view);
        Log.i(TAG, "qwer.onCreate()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "qwer.onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "qwer.onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "qwer.onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "qwer.onStop()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "qwer.onRestart()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "qwer.onDestroy()");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "qwer.onConfigurationChanged()");
    }
}
